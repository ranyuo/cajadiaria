# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
logger = logging.getLogger(__name__)

months = {
    '01':'Enero',
    '02':'Febrero',
    '03':'Marzo',
    '04':'Abril',
    '05':'Mayo',
    '06':'Junio',
    '07':'Julio',
    '08':'Agosto',
    '09':'Septiembre',
    '10':'Octubre',
    '11':'Noviembre',
    '12':'Diciembre',
}


class CashControlWizard(models.TransientModel):

    _name = "cash.control.wizard"
    _description = "Reporte para el control de caja diario"

    date = fields.Date('Fecha')
    invoice_ids = fields.Many2many('account.move', string='Facturas de Proveedores')
    total_income = fields.Float('Total de Ingresos', compute='_get_amounts')
    total_expenses = fields.Float('Total de Egresos', compute='_get_amounts')
    init_amount = fields.Float('Monto Inicial')
    company_id = fields.Many2one('res.company', string='Compañia', required=True, default=lambda self: self.env.company.id)
    line_ids = fields.One2many('cash.control.wizard.line', 'cash_control_id', string='Facturas')
    check_save = fields.Boolean('¿Fue Guardado?')

    @api.onchange('date')
    def _onchange_date(self):
        amount_aux = 0
        date_aux = False
        if self.date:
            for i in self.env['cash.control'].search([]):
                if not date_aux:
                    if i.date < self.date:
                        date_aux = i.date
                        amount_aux = i.init_amount + i.total_income + i.total_expenses
                else:
                    if i.date > date_aux and i.date < self.date:
                        date_aux = i.date
                        amount_aux = i.init_amount + i.total_income + i.total_expenses
            self.init_amount = amount_aux
            self.invoice_ids = self.env['account.move'].search([('state','=','posted'),('invoice_date', '=', self.date),('type','in',['out_invoice','in_invoice'])])
            lines = []
            self.line_ids.unlink()
            for invoice in self.invoice_ids:
                product_text = ''
                for product in invoice.invoice_line_ids:
                    product_text += product.name + ', '
                if product_text:
                    product_text = product_text[:-2]
                lines.append((0,0,{
                        'cash_control_id':self.id,
                        'invoice_id':invoice.id,
                        'name':invoice.name,
                        'products':product_text,
                        'amount':invoice.amount_total_signed,
                        'partner_id':invoice.partner_id.id,
                        'notes':invoice.narration,
                        'type':invoice.type
                }))
            self.line_ids = lines
            self._get_amounts()
        else:
            for i in self.env['cash.control'].search([]):
                if not date_aux:
                    date_aux = i.date
                    amount_aux = i.init_amount + i.total_income + i.total_expenses
                else:
                    if i.date > date_aux:
                        date_aux = i.date
                        amount_aux = i.init_amount + i.total_income + i.total_expenses
            self.init_amount = amount_aux


    def _get_amounts(self):
        self.total_expenses = 0
        self.total_income = 0
        for invoice in self.line_ids:
            if invoice.type == 'in_invoice':
                self.total_expenses += invoice.amount
            elif invoice.type == 'out_invoice':
                self.total_income += invoice.amount

    def print(self):
        return self.env.ref('cash_control_report.action_report_cash_control_wizard').report_action(self)

    def month_year(self):
        return {
                'year':str(self.date)[0:4],
                'month':months[str(self.date)[5:7]]
        }

    def save_history(self):
        if not self.check_save:
            base = self.env['cash.control'].sudo().create({
                'date':self.date,
                'invoice_ids':self.invoice_ids,
                'total_expenses':self.total_expenses,
                'total_income':self.total_income,
                'init_amount':self.init_amount,
                'company_id':self.company_id.id
            })
            lines = []
            for line in self.line_ids:
                vals = {
                    'cash_control_id': base.id,
                    'invoice_id':line.invoice_id.id,
                    'name':line.name,
                    'products':line.products,
                    'amount':line.amount,
                    'partner_id':line.partner_id.id,
                    'notes':line.notes,
                    'type':line.type
                }
                lines.append((0,0,vals))
            base.line_ids = lines
            self.check_save = True
        else:
            pass

class CashControlWizardLine(models.TransientModel):
    _name = "cash.control.wizard.line"

    cash_control_id = fields.Many2one('cash.control.wizard', string='Cash Control Base')
    invoice_id = fields.Many2one('account.move', string='Factura')
    name = fields.Char('Factura')
    products = fields.Char('Productos')
    amount = fields.Float('Monto')
    partner_id = fields.Many2one('res.partner', string='Cliente')
    notes = fields.Char('Observaciones')
    type = fields.Selection([('out_invoice','Factura de Cliente'), ('in_invoice', 'Factura de Proveedor')], string='Tipo', required=True)

