# -*- coding: utf-8 -*-
from odoo import models, fields, api
import logging
logger = logging.getLogger(__name__)

months = {
    '01':'Enero',
    '02':'Febrero',
    '03':'Marzo',
    '04':'Abril',
    '05':'Mayo',
    '06':'Junio',
    '07':'Julio',
    '08':'Agosto',
    '09':'Septiembre',
    '10':'Octubre',
    '11':'Noviembre',
    '12':'Diciembre',
}

class CashControl(models.Model):
    _name = 'cash.control'
    _rec_name = 'date'

    date = fields.Date('Fecha')
    invoice_ids = fields.Many2many('account.move', string='Facturas de Proveedores')
    total_income = fields.Float('Total de Ingresos')
    total_expenses = fields.Float('Total de Egresos')
    init_amount = fields.Float('Monto Inicial')
    company_id = fields.Many2one('res.company', string='Compañia')
    line_ids = fields.One2many('cash.control.line', 'cash_control_id', string='Facturas')

    def month_year(self):
        return {
                'year':str(self.date)[0:4],
                'month':months[str(self.date)[5:7]]
        }

class CashControlLine(models.Model):
    _name = 'cash.control.line'

    cash_control_id = fields.Many2one('cash.control', string='Base')
    invoice_id = fields.Many2one('account.move', string='Factura')
    name = fields.Char('Factura')
    products = fields.Char('Productos')
    amount = fields.Float('Monto')
    partner_id = fields.Many2one('res.partner', string='Cliente')
    notes = fields.Char('Notas')
    type = fields.Selection([('out_invoice','Facturas de Clientes'), ('in_invoice', 'Facturas de Proveedor')], string='Tipo')