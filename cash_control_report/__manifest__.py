{
    "name": "Reporte de Control de Caja ",
    "author": "Luis Millan",
    "depends": ["base", "sale", "gestionit_pe_fe"],
    "data": [
        'wizards/cash_control_wizard.xml',
        'reports/report_cash_control.xml',
        'views/cash_control.xml',
        'security/ir.model.access.csv'
    ],
}